﻿// Class voor een leverancier in het systeem, incl. enum voor type

namespace StockSysteem
{
    enum LeverancierType
    {
        Drinkwaren,
        Etenswaren,
        Olie,
        Verbruiksartikelen
    }

    class Leverancier
    {
        // Fields
        public string Naam { get; }
        public string Adres { get; }
        public LeverancierType Type { get; }

        // Constructor
        public Leverancier(string naam, string adres, LeverancierType type)
        {
            this.Naam = naam;
            this.Adres = adres;
            this.Type = type;
        }
    }
}
