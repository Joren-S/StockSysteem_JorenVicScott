﻿// Class voor een item in de stock

using System;

namespace StockSysteem
{
    class StockItem
    {
        // Static
        private static Random rand = new Random();

        // Fields
        public string Naam { get; }
        public double AankoopPrijs { get; }
        public DateTime AankoopDatum { get; }
        public DateTime VervalDatum { get; }
        public string Leverancier { get; }

        // Constructor
        public StockItem(string naam, double aankoopPrijs, string leverancier) : this(naam, aankoopPrijs, leverancier, DateTime.Now, DateTime.Now.AddDays(rand.Next(-10, 10))) { }

        public StockItem(string naam, double aankoopPrijs, string leverancier, DateTime aankoopDatum, DateTime vervalDatum)
        {
            this.Naam = naam;
            this.AankoopPrijs = aankoopPrijs;
            this.Leverancier = leverancier;
            this.AankoopDatum = aankoopDatum;
            this.VervalDatum = vervalDatum;
        }
    }
}
