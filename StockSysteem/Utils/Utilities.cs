﻿// Various utilities to speed up development

using System;

public static class Utils
{
    public static int ReadInt(string prompt, int? minVal = null, int? maxVal = null)
    {
        // We blijven doorvragen tot er een valid input is
        while (true)
        {
            // Maak de console leeg en print onze prompt                
            Console.Clear();
            Console.Write(prompt);

            // Probeer onze input als integer te interpreteren (bv. als je abc input, zal TryParse false terug geven)
            int digit;
            if (Int32.TryParse(Console.ReadLine(), out digit))
            {
                // Kijk of onze integer binnen een bepaald bereik zit (indien niet gegeven, altijd true)
                bool bigEnough = (minVal != null) ? (digit >= minVal) : true;
                bool smallEnough = (maxVal != null) ? (digit <= maxVal) : true;

                // Enkel als het groot genoeg is, geeft onze functie een waarde terug en stopt de loop
                if (bigEnough && smallEnough)
                {
                    return digit;
                }
            }
        }
    }

    public static double ReadDouble(string prompt, double? minVal = null, double? maxVal = null)
    {
        while (true)
        {            
            Console.Clear();
            Console.Write(prompt);

            double digit;
            if (Double.TryParse(Console.ReadLine(), out digit))
            {
                bool bigEnough = (minVal != null) ? (digit >= minVal) : true;
                bool smallEnough = (maxVal != null) ? (digit <= maxVal) : true;
                
                if (bigEnough && smallEnough)
                {
                    return digit;
                }
            }
        }
    }

    public static string ReadString(string prompt)
    {
        Console.Clear();
        Console.Write(prompt);
        return Console.ReadLine();
    }

    public static int[] ReadIntArray(string prompt, int len, int? minVal = null, int? maxVal = null)
    {
        if (len < 1) return null;
        int[] result = new int[len];
        for (int i = 0; i < len; i++)
        {
            result[i] = ReadInt($"{prompt}\nIndex {i}: ", minVal, maxVal);
        }
        return result;
    }

    public static void PrintArray<T>(T[] array)
    {
        Console.Write("[ ");
        for (int i = 0; i < array.Length; i++)
        {
            Console.Write(array[i]);
            if ((i + 1) != array.Length)
                Console.Write(", ");
        }
        Console.WriteLine(" ]");
    }

    private static int? Minimum(int[] arr)
    {
        int arrLen = arr.Length;
        if (arrLen < 1)
            return null;
        int min = arr[0];
        for (int i = 1; i < arrLen; i++)
        {
            min = Math.Min(min, arr[i]);
        }
        return min;
    }

    private static int? Maximum(int[] arr)
    {
        int arrLen = arr.Length;
        if (arrLen < 1)
            return null;
        int max = arr[0];
        for (int i = 1; i < arrLen; i++)
        {
            max = Math.Max(max, arr[i]);
        }
        return max;
    }
}
