﻿// Benchmark class to time code execution

using System;
using System.Diagnostics;

class Benchmark
{
    public bool running { get; set; } = false;
    public double runtime { get; set; } = 0;

    private readonly Stopwatch sw = new Stopwatch();
    private readonly string task;

    public Benchmark(string task, bool autostart = false)
    {
        this.task = task;
        if (autostart)
        {
            sw.Start();
            running = true;
        }
    }

    public bool Start()
    {
        // Check to make sure it's not already running
        if (!running)
        {
            // Reset runtime and start
            runtime = 0;
            sw.Start();
            running = true;
        }
        return running;
    }

    public double Stop(bool print = true)
    {
        // Check to make sure it's running
        if (running)
        {
            // Stop, return runtime.
            sw.Stop();
            running = false;
            if (print)
                Console.WriteLine($"{this.task} took {sw.Elapsed.TotalMilliseconds} ms");
            runtime = sw.Elapsed.TotalMilliseconds;
            return runtime;
        }
        return -1;
    }
}
