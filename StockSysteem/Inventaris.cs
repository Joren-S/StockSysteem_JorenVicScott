﻿// Main system 

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


namespace StockSysteem
{
    class Inventaris
    {
        Dictionary<string, int> maxStock = new Dictionary<string, int>();
        Dictionary<string, List<StockItem> > stock = new Dictionary<string, List<StockItem>>();

        Leverancier[] leveranciers = new Leverancier[0];

        // Main

        public void Run()
        {
            // Lees onze gegevens in
            ReadStockgroottes();
            ReadLeveranciers();
            ReadStock();

            // Zolang 'exit [4]' niet gekozen is blijven we verder doen
            while (true)
            {
                // Print main menu en bepaal keuze
                int keuze = MainMenu();

                // 1 = Stockbeheer
                if (keuze == 1)
                {
                    // Print stockbeheer menu en bepaal keuze
                    keuze = StockMenu();

                    // 1 = Bestaand item toevoegen
                    if (keuze == 1)
                    {
                        AddItem();
                    }

                    // 2 = Bestaand item verwijderen
                    else if (keuze == 2)
                    {
                        // verwijder item
                        DeleteItem();
                    }

                    // 3 = Nieuw item toevoegen
                    else if (keuze == 3)
                    {
                        AddProduct();
                    }

                    // 4 = Nieuw item verwijderen
                    else if (keuze == 4)
                    {
                        DeleteProduct();
                    }

                    // 5 = Check voor vervaldatum
                    else if (keuze == 5)
                    {
                        CheckVervalData();
                    }

                    // 6 = Pas stockgrootte aan
                    else if (keuze == 6)
                    {
                        PasStockGrootteAan();
                    }
                }

                // 2 = Leverancier beheer
                else if (keuze == 2)
                {
                    // Print leverancier menu en bepaal keuze
                    keuze = LeverancierMenu();

                    // 1 = Voeg leverancier toe
                    if (keuze == 1)
                    {
                        AddLeverancier();
                    }

                    // 2 = Verwijder leverancier
                    else if (keuze == 2)
                    {
                        DeleteLeverancier();
                    }
                }

                // 3 = Overzicht systeem
                else if (keuze == 3)
                {
                    // Print overzicht menu en bepaal keuze
                    keuze = OverzichtMenu();

                    // 1 = Print totale stock
                    if (keuze == 1)
                    {
                        PrintTotalStock();
                        Console.Read();
                    }

                    // 2 = Print alle leveranciers
                    else if (keuze == 2)
                    {
                        PrintLeveranciers();
                        Console.Read();
                    }
                }

                // 4 = Stop
                else if (keuze == 4) break;
            }
        }

        // GUI

        #region GUI

        public int MainMenu()
        {
            int keuze = Utils.ReadInt(
                  "MAIN MENU\n\n" +
                        "1. Stockbeheer\n" +
                        "2. Leverancierbeheer\n" +
                        "3. Overzicht systeem\n" +
                        "4. Exit\n" +
                "\n------------\n\n", 1, 4);
            return keuze;
        }

        public int StockMenu()
        {
            // Stock naar bestand schrijven
            int keuze = Utils.ReadInt(
                  "STOCKBEHEER\n\n" +
                        "1. Voeg item toe\n" +
                        "2. Verwijder item\n" +
                        "3. Voeg nieuw product toe\n" +
                        "4. Verwijder product uit assortiment\n" +
                        "5. Check items voor vervaldatum\n" +
                        "6. Pas stockgrootte aan\n" +
                        "7. Terug\n" +
                  "\n------------\n\n", 1, 7);
            return keuze;
        }

        public int LeverancierMenu()
        {
            // Toekomst: bestaande leveranciers aanpassen?
            int keuze = Utils.ReadInt(
                  "LEVERANCIERBEHEER\n\n" +
                        "1. Voeg leverancier toe\n" +
                        "2. Verwijder leverancier\n" +
                        "3. Terug\n" +
                  "\n------------\n\n", 1, 3);
            return keuze;
        }

        public int OverzichtMenu()
        {
            int keuze = Utils.ReadInt(
                  "OVERZICHT SYSTEEM\n\n" +
                        "1. Print stock\n" +
                        "2. Print leveranciers\n" +
                        "3. Terug\n" +
                  "\n------------\n\n", 1, 3);
            return keuze;
        }

        #endregion

        // Print functies

        #region Printing

        public void PrintItems()
        {
            // We itereren over alles binnenin onze stock dictionary
            //      -> Elk element in onze stock dictionary is van het type KeyValuePair<string, List<StockItem>>
            //      -> element.Key   verwijst naar de string
            //      -> element.Value verwijst naar de List<StockItem>
            int count = 0;
            foreach (KeyValuePair<string, List<StockItem>> element in stock)
            {
                Console.Write(element.Key + " ");
                count++;

                // We printen 5 per lijn, dus na 5 elementen een newline en count terug naar 0.
                if (count == 5)
                {
                    count = 0;
                    Console.Write(Environment.NewLine);
                }
            }
            Console.Write(Environment.NewLine);
        }

        public void PrintStock(string item)
        {
            // We kijken of het item in onze stock bestaat en printen indien zo.
            if (stock.ContainsKey(item))
            {
                Console.WriteLine($"{item}: \n\t{stock[item].Count} / {maxStock[item]}");
            }
        }

        public void PrintTotalStock()
        {
            // We itereren over alles binnenin onze stock dictionary zoals in PrintItems()
            Console.Clear();
            Console.WriteLine("STOCK\n");
            foreach (KeyValuePair<string, List<StockItem>> stockItem in stock)
            {
                // We gebruiken de bovenstaande functie PrintStock(string) voor onze afzonderlijke entries.
                PrintStock(stockItem.Key);
            }
            Console.WriteLine("\n-------------\n");
        }

        public void PrintLeveranciers()
        {
            Console.Clear();
            Console.WriteLine("LEVERANCIERS\n");
            
            // We sorteren onze leveranciers op basis van het 'Type' property van Leverancier
            leveranciers = leveranciers.OrderBy(c => c.Type).ToArray();
            
            // We itereren over onze leveranciers array
            foreach (Leverancier currentLeverancier in leveranciers)
            {
                // Als het einde (null) is bereikt, stoppen we.
                if (currentLeverancier == null)
                    break;
                Console.WriteLine($"{currentLeverancier.Naam}: {currentLeverancier.Type.ToString()}");
            }
            Console.WriteLine("\n-------------\n");
        }

        #endregion

        // functies

        #region Functions

        public void AddItem()
        {
            // We blijven doorvragen tot er een geldig item is ingevoerd
            string keuze;
            while (true)
            {
                // We maken het venster leeg, geven alle mogelijke items weer en vragen om input
                Console.Clear();
                PrintItems();
                Console.WriteLine("\n----------------\n\n" +
                                  "Aan welk item wilt u toevoegen?");
                keuze = Console.ReadLine();
                
                // Als het item niet in onze stock bestaat -> error en we blijven in de while-loop
                if (!stock.ContainsKey(keuze))
                {
                    Console.WriteLine("Het ingevoerde item bestaat niet in het systeem.");
                    Console.ReadLine();
                }
                // Indien het wel bestaat, break uit de while-loop
                else break;
            }
            
            // We maken het venster leeg en vragen het aantal, de aankoopprijs en de leverancier
            Console.Clear();
            int count = Utils.ReadInt($"Hoeveel [{keuze}] wilt u toevoegen?\n", 0);         // ???? set maximum
            double aankoopPrijs = Utils.ReadDouble($"Wat is de aankoopprijs per [{keuze}]?\n", 0); // ???? set maximum
            string leverancierNaam = Utils.ReadString("Wie is de leverancier?\n");

            // We voegen het correct aantal items toe (count)
            for (int i = 0; i < count; i++)
            {
                // Voor elk item maken we een nieuw StockItem object en we voegen dit object toe aan onze StockItem List
                // Onze stock is een Dictionary<string, List<StockItem> >, dus:
                //      -> stock[keuze] is een List<StockItem>
                //      -> stock[keuze].Add() voegt achteraan onze list toe.
                
                int capacityRemaining = maxStock[keuze] - stock[keuze].Count;
                if (capacityRemaining > 0)
                {
                    // voeg huidige toe
                    StockItem newItem = new StockItem(keuze, aankoopPrijs, leverancierNaam);
                    stock[keuze].Add(newItem);
                }
                else
                {
                    Console.WriteLine($"Het maximum aantal [{keuze}] is bereikt.");
                    Console.ReadLine();
                    break;
                }
            }

            // Sorteer de stock op basis van de vervaldatum
            stock[keuze] = stock[keuze].OrderBy(x => x.VervalDatum).ToList();
            
            // Schrijf weg naar ons bestand
            WriteStock();
        }

        public void DeleteItem()
        {
            // We blijven doorvragen tot er een geldig item is ingevoerd
            string keuze;
            while (true)
            {
                // We maken het venster leeg, geven alle mogelijke items weer en vragen om input
                Console.Clear();
                PrintItems();
                Console.WriteLine("\n----------------\n\n" +
                                  "Van welk item wil je de stock verlagen?");
                keuze = Console.ReadLine();

                // Als het item niet in onze stock bestaat -> error en we blijven in de while-loop
                if (!stock.ContainsKey(keuze))
                {
                    Console.WriteLine("Het ingevoerde item bestaat niet in het systeem.");
                    Console.ReadLine();
                }
                // Indien het wel bestaat, break uit de while-loop
                else break;
            }

            // We maken het venster leeg en vragen het aantal te verwijderen.
            // We kijken ook hoeveel er effectief nog in de stock zitten
            Console.Clear();
            int count = Utils.ReadInt($"Hoeveel [{keuze}] wilt u verwijderen?\n", 0);
            int available = stock[keuze].Count;

            // Indien er niet genoeg in stock is, krijgen we een keuze:
            if (count > available)
            {
                Console.Clear();
                
                // Indien er niet genoeg is, maar wel meer als 0:
                if (available > 0)
                {
                    int menuKeuze = Utils.ReadInt($"Er zijn slechts {available}x [{keuze}] in stock." +
                                               $"\n1. Gebruik beschikbare hoeveelheid" +
                                               $"\n2. Annuleer\n", 1, 2);

                    if (menuKeuze == 1)
                    {
                        // RemoveRange verwijderd 'count' aantal elementen in volgorde, beginnende van 'index'
                        // RemoveRange(0, available) zal dus de eerste 'available' elementen verwijderen
                        stock[keuze].RemoveRange(0, available);
                    }
                    else Console.Clear();
                }
                // Indien er niets meer in stock is:
                else
                {
                    Console.WriteLine($"Het product [{keuze}] is niet meer in voorraad.");
                    Console.ReadLine();
                }
            }
            else
            {
                // Indien er voldoende zijn, verwijder 'count' vooraan in de stock
                // (vooraan = snelste vervaldatum)
                stock[keuze].RemoveRange(0, count);
            }

            WriteStock();
        }

        public void AddProduct()
        {
            // We blijven doorvragen tot 'stop' of tot valid input
            string input = null;
            while (true)
            {
                // We maken het venster leeg en printen alle huidige item namen
                Console.Clear();
                PrintItems();
                
                // We vragen de naam van het nieuwe product, normalizeren naar lowercase en trimmen eventuele spaties/whitespace
                Console.Write("\n----------------\n\n" +
                              "Geef de naam van het nieuwe product." +
                              "\nGeef \"stop\" in om te stoppen:\n");
                input = Console.ReadLine().ToLower().Trim();

                // We checken of de input er een valid string is
                if (!string.IsNullOrWhiteSpace(input))
                {
                    // Stop -> break uit de while loop
                    if (input == "stop") break;

                    // Al de input al bestaat in onze stock -> error en vraag opnieuw
                    if (stock.ContainsKey(input))
                    {
                        Console.WriteLine($"Het product [{input}] bestaat reeds in het assortiment.");
                        Console.ReadLine();
                    }

                    // Anders: voeg toe aan onze stock en schrijf dit meteen weg naar ons stock bestand 
                    else
                    {
                        stock.Add(input, new List<StockItem>());
                        if (!maxStock.ContainsKey(input))
                        {
                            maxStock.Add(input, 0);
                        }

                        Console.WriteLine($"[{input}] is aan het systeem toegevoegd.");
                        Console.ReadLine();
                        WriteStock();
                        break;
                    }
                }
            }
        }
        
        public void DeleteProduct()
        {
            // We blijven doorvragen tot 'stop' of tot valid input
            string input = null;
            while (true)
            {
                // Maak venster leeg en print alle huidige item namen
                Console.Clear();
                PrintItems();

                // Vraag voor input, normalizeer naar lowercase en trim eventuele spaties/whitespace
                Console.Write("\n----------------\n\n" +
                              "Geef de naam van het te verwijderen product." +
                              "\nGeef \"stop\" in om te stoppen:\n");
                input = Console.ReadLine().ToLower().Trim();

                // Check of input een valid string is
                if (!string.IsNullOrWhiteSpace(input))
                {
                    // Stop -> break uit de while loop
                    if (input == "stop") break;

                    // Als input niet bestaat in onze stock -> error message en vraag opnieuw
                    if (!stock.ContainsKey(input))
                    {
                        Console.WriteLine($"Het product [{input}] bestaat niet in het systeem.");
                        Console.ReadLine();
                    }

                    // Indien het wel bestaat in onze stock
                    else
                    {
                        // Verwijder uit de stock en schrijf ook weg naar ons bestand en break uit de while loop
                        stock.Remove(input);
                        WriteStock();
                        Console.WriteLine($"[{input}] is uit het systeem verwijderd.");
                        Console.ReadLine();
                        break;
                    }
                }
            }
        }
        
        public void AddLeverancier()
        {
            // We blijven doorvragen tot valid input is gegeven
            string input;
            while (true)
            {
                // Maak het venster leeg en print alle huidige leverancier namen
                Console.Clear();
                PrintLeveranciers();

                // Vraag om input en trim eventuele whitespace/spaties
                Console.Write("Geef de naam van de nieuwe leverancier:\n");
                input = Console.ReadLine().Trim();

                // Check of onze input een valid string is
                if (!string.IsNullOrWhiteSpace(input))
                {
                    // Itereren door onze leveranciers array om te kijken of onze input reeds bestaat
                    bool exists = false;
                    foreach (Leverancier curLeverancier in leveranciers)
                    {
                        if (curLeverancier != null)
                        {
                            if (curLeverancier.Naam == input)
                            {
                                exists = true;
                                break;
                            }
                        }
                    }

                    // Indien de leverancier al bestaat -> error message + vraag opnieuw
                    if (exists)
                    {
                        Console.WriteLine($"[{input}] bestaat reeds in het systeem.");
                        Console.ReadLine();
                    }

                    // Indien de leverancier niet bestaat
                    else
                    {
                        // Input het adres van de leverancier
                        string adres = Utils.ReadString($"Geef het adres van [{input}]:\n");

                        // Print alle leverancier types en input het type van de nieuwe leverancier
                        int count = 0;
                        string question = "";
                        foreach (LeverancierType type in Enum.GetValues(typeof(LeverancierType)))
                        {
                            count++;
                            question += $"{count}: {(LeverancierType)type}\n";
                        }
                        question += $"\nGeef het type van [{input}]:\n";
                        LeverancierType typeInput = (LeverancierType)Utils.ReadInt(question, 1, count);

                        // Resize onze leverancier array naar 1 groter en plaats onze nieuwe leverancier daar
                        Array.Resize(ref leveranciers, leveranciers.Length + 1);
                        leveranciers[leveranciers.Length - 1] = new Leverancier(input, adres, typeInput - 1);
                        Console.WriteLine($"[{input}] is toegevoegd aan het systeem.");

                        // Schrijf weg naar ons bestand en break uit de while loop
                        WriteLeveranciers();
                        break;
                    }
                }
            }
        }

        public void DeleteLeverancier()
        {
            // Blijf doorvragen tot valid input is gegeven
            string input;
            while (true)
            {
                // Maak venster leeg en print alle huidige leverancier namen
                Console.Clear();
                PrintLeveranciers();

                // Input de naam van de te verwijderen leverancier en trim eventuele spaties/whitespaces
                Console.Write("Geef de naam van de te verwijderen leverancier:\n");
                input = Console.ReadLine().Trim();

                // Indien input een valid string is
                if (!string.IsNullOrWhiteSpace(input))
                {
                    int index = 0;
                    bool deleted = false;

                    // Itereer over onze leveranciers array
                    foreach (Leverancier curLeverancier in leveranciers)
                    {
                        // Check om zeker te zijn dat ons object bestaat
                        if (curLeverancier != null)
                        {
                            if (curLeverancier.Naam == input)
                            {
                                // Indien de te verwijderen leverancier gevonden is, verwijderen we dit element uit onze array
                                leveranciers[index] = null;

                                // We gaan nu alles naar links opschuiven zodat er geen 'null' in het midden van onze array staat
                                // Startende van dit element (dat nu null is), tot het einde van onze array - 1:
                                for (int i = index; i < leveranciers.Length - 1; i++)
                                {
                                    // Schuif op naar links
                                    leveranciers[i] = leveranciers[i + 1];
                                }
                                
                                // We resizen onze array naar 1 kleiner
                                Array.Resize(ref leveranciers, leveranciers.Length - 1);

                                // We duiden aan dat het verwijderen gelukt is en breaken uit onze foreach loop
                                deleted = true;
                                break;
                            }
                        }
                        index++;
                    }

                    // Indien verwijderd:
                    if (deleted)
                    {
                        // Schrijf weg naar ons bestand en break uit de while loop
                        WriteLeveranciers();
                        break;
                    }
                }
            }
        }

        public void PasStockGrootteAan()
        {
            // We blijven doorvragen tot er een geldig item is ingevoerd
            string keuze;
            while (true)
            {
                // We maken het venster leeg, geven alle mogelijke items weer en vragen om input
                Console.Clear();
                PrintItems();
                Console.WriteLine("\n----------------\n\n" +
                                  "Van welk item wilt u de stockgrootte aanpassen?");
                keuze = Console.ReadLine();

                // Als het item effectief in onze stock bestaat:
                if (stock.ContainsKey(keuze))
                {
                    // Als het item nog geen max stock heeft toegewezen:
                    if (!maxStock.ContainsKey(keuze))
                    {
                        maxStock.Add(keuze, 0);
                    }
                    
                    // Break uit de while loop
                    break;
                }
            }
            
            // Input en pas onze max stock aan
            // Minimum stock grootte is het aantal elementen reeds in deze stock
            maxStock[keuze] = Utils.ReadInt($"Wat is de nieuwe grootte van de [{keuze}] stock?\n", stock[keuze].Count);

            // Schrijf weg naar ons bestand
            WriteStockgroottes();
        }

        public void CheckVervalData()
        {
            Console.Clear();

            // Itereer over alle verschillende items in de stock
            foreach (KeyValuePair<string, List<StockItem>> stockItem in stock)
            {
                // Tel hoeveel er bijna vervallen (< 24h) en hoeveel er al vervallen zijn
                int countWarning = 0;
                int countVervallen = 0;

                // We itereren over alle verschillende StockItems in onze specifieke item stock 
                foreach (StockItem item in stockItem.Value)
                {
                    // Bepaal tijd tot vervaldatum
                    TimeSpan remaining = item.VervalDatum - DateTime.Now;

                    // Indien vervaldatum binnen 24 uur, maar wel in de toekomst
                    if (remaining.TotalHours < 24 && remaining.TotalMilliseconds > 0)
                    {
                        countWarning++;
                    }
                    
                    // Indien vervaldatum in het verleden
                    if (remaining.TotalMilliseconds < 0)
                    {
                        countVervallen++;
                    }
                }

                // Print indien nodig
                if (countWarning > 0)
                {
                    Console.WriteLine($"[{stockItem.Key}]: {countWarning}x {(countWarning == 1? "vervalt" : "vervallen")} binnen 24h.");
                }

                if (countVervallen > 0)
                {
                    Console.WriteLine(
                        $"[{stockItem.Key}]: {countVervallen}x {(countVervallen == 1 ? "is" : "zijn")} vervallen.");
                }
                if (countVervallen == 0 && countWarning == 0)
                {
                    Console.WriteLine($"[{stockItem.Key}]: OK");
                }
            }

            Console.ReadLine();
        }

        #endregion

        // File IO

        #region File IO

        public void ReadLeveranciers(string LeverancierPath = "leveranciers.csv")
        {
            // We kijken of het leveranciers bestand wel bestaat
            if (File.Exists(LeverancierPath))
            {
                // Indien ja, openen we dit bestand met onze StreamReader
                string curLine;
                StreamReader read = new StreamReader(LeverancierPath);

                // Zolang het einde van het bestand/stream nog niet bereikt is, blijven we in de while-loop
                while (!read.EndOfStream)
                {
                    // We lezen de eerstvolgende ongelezen lijn en splitsen deze lijn op de ','
                    curLine = read.ReadLine();
                    string[] fields = curLine.Split(',');

                    // We resizen onze array naar grootte + 1
                    // En plaatsen onze nieuwe leverancier op het einde
                    Array.Resize(ref leveranciers, leveranciers.Length + 1);
                    leveranciers[leveranciers.Length - 1] = new Leverancier(fields[0], fields[1], (LeverancierType)Convert.ToInt32(fields[2]));

                }

                // We sluiten onze StreamReader
                read.Close();
            }
        }

        public void WriteLeveranciers(string LeverancierPath = "leveranciers.csv")
        {
            // Gebruikmakend van onze StremWriter:
            // (using -> automatische opkuis als we klaar zijn)
            using (StreamWriter writer = new StreamWriter(LeverancierPath))
            {
                // We itereren over onze leveranciers array
                for (int i = 0; i < leveranciers.Length; i++)
                {
                    // Indien er een leverancier gevonden is:
                    if (leveranciers[i] != null)
                    {
                        // csv per lijn:
                        // [naam],[adres],[type]
                        writer.WriteLine($"{leveranciers[i].Naam},{leveranciers[i].Adres},{(int)leveranciers[i].Type}");
                    }
                }
            }
        }

        public void ReadStockgroottes(string maxStockPath = "stockgrootte.csv")
        {
            if (File.Exists(maxStockPath))
            {
                string curLine;
                StreamReader read = new StreamReader(maxStockPath);
                
                while (!read.EndOfStream)
                {
                    curLine = read.ReadLine();
                    string[] fields = curLine.Split(',');

                    if (!stock.ContainsKey(fields[0]))
                    {
                        stock.Add(fields[0], new List<StockItem>());
                    }

                    if (!maxStock.ContainsKey(fields[0]))
                    {
                        maxStock.Add(fields[0], 0);
                    }
                    maxStock[fields[0]] = Convert.ToInt32(fields[1]);
                }
                read.Close();
            }
        }

        public void WriteStockgroottes(string maxStockPath = "stockgrootte.csv")
        {
            using (StreamWriter writer = new StreamWriter(maxStockPath))
            {
                foreach (KeyValuePair<string, int> item in maxStock)
                {
                    writer.WriteLine($"{item.Key},{item.Value}");
                }
            }
        }
        
        public void ReadStock(string stockPath = "stock.csv")
        {
            if (File.Exists(stockPath))
            {
                string curLine;
                StreamReader reader = new StreamReader(stockPath);
                while (!reader.EndOfStream)
                {
                    curLine = reader.ReadLine();
                    string[] splitLine = curLine.Split(',');
                    
                    stock[splitLine[0]].Add(
                        new StockItem(splitLine[1], Convert.ToDouble(splitLine[2]), splitLine[3],
                            Convert.ToDateTime(splitLine[4]), Convert.ToDateTime(splitLine[5]))
                        );
                }
                reader.Close();
            }
        }

        public void WriteStock(string stockPath = "stock.csv")
        {
            using (StreamWriter writer = new StreamWriter(stockPath))
            {
                foreach (KeyValuePair<string, List<StockItem>> stockItem in stock)
                {
                    foreach (StockItem item in stockItem.Value)
                    {
                        writer.WriteLine($"{stockItem.Key},{item.Naam},{item.AankoopPrijs},{item.Leverancier},{item.AankoopDatum},{item.VervalDatum}");
                    }
                }
            }
        }

        // Stock

        #endregion
    }
}